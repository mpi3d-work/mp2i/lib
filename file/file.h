#include <stdbool.h>

#ifndef FILE_H
#define FILE_H

typedef int contenu;

typedef struct file_s *file;

file creer_file(void);

void enfiler_file(file, contenu);

contenu defiler_file(file);

bool est_vide_file(file);

void detruire_file(file);

#endif
