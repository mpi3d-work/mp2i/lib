#include <assert.h>
#include <stdlib.h>

#include "file.h"

struct maillon_s
{
    contenu valeur;
    struct maillon_s *suivant;
};

typedef struct maillon_s *maillon;

struct file_s
{
    maillon premier;
    maillon dernier;
};

file creer_file(void)
{
    file f = malloc(sizeof(struct file_s));
    assert(f != NULL);

    f->premier = NULL;
    f->dernier = NULL;

    return f;
}

void enfiler_file(file f, contenu c)
{
    maillon m = malloc(sizeof(struct maillon_s));
    assert(m != NULL);

    m->valeur = c;
    m->suivant = NULL;

    if (f->dernier != NULL)
        f->dernier->suivant = m;
    else
        f->premier = m;

    f->dernier = m;
}

contenu defiler_file(file f)
{
    assert(!est_vide_file(f));

    maillon m = f->premier;
    contenu c = m->valeur;

    f->premier = m->suivant;
    if (f->premier == NULL)
        f->dernier = NULL;

    free(m);

    return c;
}

bool est_vide_file(file f)
{
    return f->premier == NULL && f->dernier == NULL;
}

void detruire_file(file f)
{
    while (!est_vide_file(f))
        defiler_file(f);

    free(f);
}
