let rec binarise_foret = function
  | x :: l ->
      Binaire.Noeud
        (x.Quelconque.etiquette, binarise_foret x.fils, binarise_foret l)
  | [] -> Binaire.Vide

let binarise a = binarise_foret [ a ]

let rec debinarise_foret = function
  | Binaire.Noeud (e, a, b) ->
      { Quelconque.etiquette = e; Quelconque.fils = debinarise_foret a }
      :: debinarise_foret b
  | Binaire.Vide -> []

let debinarise a =
  match debinarise_foret a with
  | [ x ] -> x
  | _ -> raise (Invalid_argument "arbre invalide")
