let rec parcours_profondeur_prefixe f a =
  f a.Arbre.etiquette;
  parcours_profondeur_prefixe_foret f a.Arbre.fils

and parcours_profondeur_prefixe_foret f = function
  | a :: l ->
      parcours_profondeur_prefixe f a;
      parcours_profondeur_prefixe_foret f l
  | [] -> ()

let rec parcours_profondeur_postfixe f a =
  parcours_profondeur_postfixe_foret f a.Arbre.fils;
  f a.Arbre.etiquette

and parcours_profondeur_postfixe_foret f = function
  | a :: l ->
      parcours_profondeur_postfixe_foret f l;
      parcours_profondeur_postfixe f a
  | [] -> ()

let rec remplir_file f = function
  | x :: l ->
      Queue.push x f;
      remplir_file f l
  | [] -> ()

let parcours_largeur f a =
  let g = Queue.create () in
  Queue.push a g;
  while not (Queue.is_empty g) do
    let a = Queue.pop g in
    f a.Arbre.etiquette;
    remplir_file g a.Arbre.fils
  done
