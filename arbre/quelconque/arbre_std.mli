val taille : 'a Arbre.arbre -> int
val hauteur : 'a Arbre.arbre -> int
val est_feuille : 'a Arbre.arbre -> bool
val nombre_feuilles : 'a Arbre.arbre -> int
