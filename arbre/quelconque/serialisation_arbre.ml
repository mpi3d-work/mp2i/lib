let rec serialise f c a = f c a.Arbre.etiquette

and serialise_foret f c = function
  | a :: l ->
      output_string c "1\n";
      serialise f c a;
      serialise_foret f c l
  | [] -> output_string c "0\n"

let rec deserialise f c =
  { Arbre.etiquette = f c; Arbre.fils = deserialise_foret f c }

and deserialise_foret f c =
  if int_of_string (input_line c) <> 0 then
    deserialise f c :: deserialise_foret f c
  else []
