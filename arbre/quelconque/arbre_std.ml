let rec taille a = 1 + taille_foret a.Arbre.fils
and taille_foret = function a :: l -> taille a + taille_foret l | [] -> 0

let max a b = if a > b then a else b

let rec hauteur a = 1 + hauteur_foret a.Arbre.fils

and hauteur_foret = function
  | a :: l -> max (hauteur a) (hauteur_foret l)
  | [] -> 0

let est_feuille a = a.Arbre.fils = []

let rec nombre_feuilles a =
  if a.Arbre.fils = [] then 1 else nombre_feuilles_foret a.Arbre.fils

and nombre_feuilles_foret = function
  | a :: l -> nombre_feuilles a + nombre_feuilles_foret l
  | [] -> 0
