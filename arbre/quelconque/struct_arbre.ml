let rec egaux a b =
  a.Arbre.etiquette = b.Arbre.etiquette && egaux_foret a.Arbre.fils b.Arbre.fils

and egaux_foret a b =
  match (a, b) with
  | a :: l, b :: m -> egaux a b && egaux_foret l m
  | [], [] -> true
  | _ -> false
(* Pour les flemmards *)
(* let egaux a b = a = b *)

let rec appartient a x =
  a.Arbre.etiquette = x || appartient_foret a.Arbre.fils x

and appartient_foret l x =
  match l with a :: l -> appartient a x || appartient_foret l x | [] -> false
