val parcours_profondeur_prefixe : ('a -> 'b) -> 'a Arbre.arbre -> unit
val parcours_profondeur_postfixe : ('a -> unit) -> 'a Arbre.arbre -> unit
val parcours_largeur : ('a -> unit) -> 'a Arbre.arbre -> unit
