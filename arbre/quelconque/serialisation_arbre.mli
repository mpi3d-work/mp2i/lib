val serialise :
  (out_channel -> 'a -> unit) -> out_channel -> 'a Arbre.arbre -> unit

val deserialise : (in_channel -> 'a) -> in_channel -> 'a Arbre.arbre
