#include <assert.h>

#include "arbre_std.h"

#include "test_exemple_arbre.h"

void test_taille_arbre(void)
{
    arbre a;

    a = arbre_0();
    assert(taille_arbre(a) == 0);
    detruire_arbre(a);

    a = arbre_1();
    assert(taille_arbre(a) == 1);
    detruire_arbre(a);

    a = arbre_2();
    assert(taille_arbre(a) == 2);
    detruire_arbre(a);

    a = arbre_3();
    assert(taille_arbre(a) == 2);
    detruire_arbre(a);

    a = arbre_4();
    assert(taille_arbre(a) == 7);
    detruire_arbre(a);

    a = arbre_5();
    assert(taille_arbre(a) == 9);
    detruire_arbre(a);
}

void test_hauteur_arbre(void)
{
    arbre a;

    a = arbre_0();
    assert(hauteur_arbre(a) == 0);
    detruire_arbre(a);

    a = arbre_1();
    assert(hauteur_arbre(a) == 1);
    detruire_arbre(a);

    a = arbre_2();
    assert(hauteur_arbre(a) == 2);
    detruire_arbre(a);

    a = arbre_3();
    assert(hauteur_arbre(a) == 2);
    detruire_arbre(a);

    a = arbre_4();
    assert(hauteur_arbre(a) == 4);
    detruire_arbre(a);

    a = arbre_5();
    assert(hauteur_arbre(a) == 4);
    detruire_arbre(a);
}

void test_est_feuille_arbre(void)
{
    arbre a;

    a = arbre_0();
    assert(!est_feuille_arbre(a));
    detruire_arbre(a);

    a = arbre_1();
    assert(est_feuille_arbre(a));
    detruire_arbre(a);

    a = arbre_2();
    assert(!est_feuille_arbre(a));
    detruire_arbre(a);

    a = arbre_3();
    assert(!est_feuille_arbre(a));
    detruire_arbre(a);
}

void test_nombre_feuilles_arbre(void)
{
    arbre a;

    a = arbre_0();
    assert(nombre_feuilles_arbre(a) == 0);
    detruire_arbre(a);

    a = arbre_1();
    assert(nombre_feuilles_arbre(a) == 1);
    detruire_arbre(a);

    a = arbre_2();
    assert(nombre_feuilles_arbre(a) == 1);
    detruire_arbre(a);

    a = arbre_3();
    assert(nombre_feuilles_arbre(a) == 1);
    detruire_arbre(a);

    a = arbre_4();
    assert(nombre_feuilles_arbre(a) == 3);
    detruire_arbre(a);

    a = arbre_5();
    assert(nombre_feuilles_arbre(a) == 5);
    detruire_arbre(a);
}

void test_est_strict_arbre(void)
{
    arbre a;

    a = arbre_0();
    assert(!est_strict_arbre(a));
    detruire_arbre(a);

    a = arbre_1();
    assert(est_strict_arbre(a));
    detruire_arbre(a);

    a = arbre_2();
    assert(!est_strict_arbre(a));
    detruire_arbre(a);

    a = arbre_3();
    assert(!est_strict_arbre(a));
    detruire_arbre(a);

    a = arbre_4();
    assert(!est_strict_arbre(a));
    detruire_arbre(a);

    a = arbre_5();
    assert(est_strict_arbre(a));
    detruire_arbre(a);
}

void test_arbre_std(void)
{
    test_taille_arbre();
    test_hauteur_arbre();
    test_est_feuille_arbre();
    test_nombre_feuilles_arbre();
    test_est_strict_arbre();
}
