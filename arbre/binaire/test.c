#include "test_arbre.h"
#include "test_arbre_std.h"
#include "test_parcours_arbre.h"
#include "test_struct_arbre.h"
#include "test_serialisation_arbre.h"

int main(void)
{
    test_arbre();
    test_arbre_std();
    test_parcours_arbre();
    test_struct_arbre();
    test_serialisation_arbre();
    return 0;
}
