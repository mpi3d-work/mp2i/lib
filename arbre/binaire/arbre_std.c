#include "arbre_std.h"

int taille_arbre(arbre a)
{
    if (est_vide_arbre(a))
        return 0;
    return 1 +
           taille_arbre(gauche_arbre(a)) +
           taille_arbre(droite_arbre(a));
}

int max(int a, int b)
{
    if (a > b)
        return a;
    return b;
}

int hauteur_arbre(arbre a)
{
    if (est_vide_arbre(a))
        return 0;
    return 1 + max(
                   hauteur_arbre(gauche_arbre(a)),
                   hauteur_arbre(droite_arbre(a)));
}

bool est_feuille_arbre(arbre a)
{
    return !est_vide_arbre(a) &&
           est_vide_arbre(gauche_arbre(a)) &&
           est_vide_arbre(droite_arbre(a));
}

int nombre_feuilles_arbre(arbre a)
{
    if (est_vide_arbre(a))
        return 0;
    arbre g = gauche_arbre(a);
    arbre d = droite_arbre(a);
    if (est_vide_arbre(g) && est_vide_arbre(d))
        return 1;
    return nombre_feuilles_arbre(g) + nombre_feuilles_arbre(d);
}

bool est_strict_arbre(arbre a)
{
    if (est_vide_arbre(a))
        return false;
    arbre g = gauche_arbre(a);
    arbre d = droite_arbre(a);
    if (est_vide_arbre(g) && est_vide_arbre(d))
        return true;
    return est_strict_arbre(g) && est_strict_arbre(d);
}
