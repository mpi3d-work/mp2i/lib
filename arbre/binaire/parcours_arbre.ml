let rec parcours_profondeur_prefixe f = function
  | Arbre.Noeud (e, a, b) ->
      f e;
      parcours_profondeur_prefixe f a;
      parcours_profondeur_prefixe f b
  | Arbre.Vide -> ()

let rec parcours_profondeur_infixe f = function
  | Arbre.Noeud (e, a, b) ->
      parcours_profondeur_infixe f a;
      f e;
      parcours_profondeur_infixe f b
  | Arbre.Vide -> ()

let rec parcours_profondeur_postfixe f = function
  | Arbre.Noeud (e, a, b) ->
      parcours_profondeur_postfixe f a;
      parcours_profondeur_postfixe f b;
      f e
  | Arbre.Vide -> ()

let parcours_largeur f a =
  let g = Queue.create () in
  Queue.push a g;
  while not (Queue.is_empty g) do
    match Queue.pop g with
    | Arbre.Noeud (e, a, b) ->
        Queue.push a g;
        Queue.push b g;
        f e
    | Arbre.Vide -> ()
  done
