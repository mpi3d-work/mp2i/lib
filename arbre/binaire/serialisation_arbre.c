#include "serialisation_arbre.h"

int serialise_arbre(FILE *f, arbre a)
{
    int n = 0;

    bool t = !est_vide_arbre(a);

    int r = fprintf(f, "%d\n", t);
    if (r < 0)
        return r;
    n += r;

    if (t)
    {
        r = serialise_contenu(f, etiquette_arbre(a));
        if (r < 0)
            return r;
        n += r;

        r = serialise_arbre(f, gauche_arbre(a));
        if (r < 0)
            return r;
        n += r;

        r = serialise_arbre(f, droite_arbre(a));
        if (r < 0)
            return r;
        n += r;
    }

    return n;
}

int cap(int r)
{
    if (r < 0)
        return r;
    return 0;
}

int deserialise_arbre(FILE *f, arbre *a)
{
    int t;
    int r = fscanf(f, "%d\n", &t);
    if (r != 1)
        return cap(r);

    if (t != 0)
    {
        contenu c;
        r = deserialise_contenu(f, &c);
        if (r != 1)
            return cap(r);

        arbre g;
        r = deserialise_arbre(f, &g);
        if (r != 1)
            return cap(r);

        arbre d;
        r = deserialise_arbre(f, &d);
        if (r != 1)
            return cap(r);

        *a = ajouter_arbre(g, d, c);
    }
    else
        *a = creer_arbre();

    return 1;
}
