let rec serialise f c = function
  | Arbre.Noeud (e, a, b) ->
      output_string c "1\n";
      f c e;
      serialise f c a;
      serialise f c a
  | Arbre.Vide -> output_string c "0\n"

let rec deserialise f c =
  if int_of_string (input_line c) <> 0 then
    Arbre.Noeud (f c, deserialise f c, deserialise f c)
  else Arbre.Vide
