#include <assert.h>
#include <stdlib.h>

#include "parcours_arbre.h"

void parcours_profondeur_prefixe(arbre a)
{
    if (est_vide_arbre(a))
        return;
    parcours_contenu(etiquette_arbre(a));
    parcours_profondeur_prefixe(gauche_arbre(a));
    parcours_profondeur_prefixe(droite_arbre(a));
}

void parcours_profondeur_infixe(arbre a)
{
    if (est_vide_arbre(a))
        return;
    parcours_profondeur_infixe(gauche_arbre(a));
    parcours_contenu(etiquette_arbre(a));
    parcours_profondeur_infixe(droite_arbre(a));
}

void parcours_profondeur_postfixe(arbre a)
{
    if (est_vide_arbre(a))
        return;
    parcours_profondeur_postfixe(gauche_arbre(a));
    parcours_profondeur_postfixe(droite_arbre(a));
    parcours_contenu(etiquette_arbre(a));
}

struct maillon_s
{
    arbre a;
    struct maillon_s *s;
};

typedef struct maillon_s *maillon;

maillon creer_maillon(arbre a)
{
    maillon m = malloc(sizeof(struct maillon_s));
    assert(m != NULL);

    m->a = a;
    m->s = NULL;

    return m;
}

void parcours_largeur(arbre a)
{
    maillon p = creer_maillon(a);
    maillon d = p;

    while (p != NULL)
    {
        maillon m = p;

        arbre a = m->a;
        if (!est_vide_arbre(a))
        {
            parcours_contenu(etiquette_arbre(a));
            d->s = creer_maillon(gauche_arbre(a));
            d = d->s;
            d->s = creer_maillon(droite_arbre(a));
            d = d->s;
        }

        p = p->s;
        free(m);
    }
}
