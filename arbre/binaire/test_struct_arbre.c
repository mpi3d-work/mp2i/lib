#include <assert.h>

#include "struct_arbre.h"

#include "test_exemple_arbre.h"

bool egaux_contenu(contenu a, contenu b)
{
    return a == b;
}

void test_egaux_arbre(void)
{
    arbre a = arbre_0();
    arbre b = arbre_0();
    assert(egaux_arbre(a, b));
    detruire_arbre(a);

    a = arbre_1();
    assert(!egaux_arbre(a, b));
    detruire_arbre(b);
    b = arbre_1();
    assert(egaux_arbre(a, b));
    detruire_arbre(a);

    a = arbre_2();
    assert(!egaux_arbre(a, b));
    detruire_arbre(b);
    b = arbre_2();
    assert(egaux_arbre(a, b));
    detruire_arbre(a);

    a = arbre_3();
    assert(!egaux_arbre(a, b));
    detruire_arbre(b);
    b = arbre_3();
    assert(egaux_arbre(a, b));
    detruire_arbre(a);

    a = arbre_4();
    assert(!egaux_arbre(a, b));
    detruire_arbre(b);
    b = arbre_4();
    assert(egaux_arbre(a, b));
    detruire_arbre(a);

    a = arbre_5();
    assert(!egaux_arbre(a, b));
    detruire_arbre(b);
    b = arbre_5();
    assert(egaux_arbre(a, b));
    detruire_arbre(a);

    detruire_arbre(b);
}

void test_appartient_arbre(void)
{
    arbre a;

    a = arbre_0();
    assert(!appartient_arbre(a, 0));
    assert(!appartient_arbre(a, 1));
    detruire_arbre(a);

    a = arbre_1();
    assert(!appartient_arbre(a, 0));
    assert(appartient_arbre(a, 1));
    detruire_arbre(a);

    a = arbre_2();
    assert(!appartient_arbre(a, 0));
    assert(appartient_arbre(a, 1));
    assert(appartient_arbre(a, 2));
    detruire_arbre(a);

    a = arbre_3();
    assert(!appartient_arbre(a, 0));
    assert(appartient_arbre(a, 1));
    assert(appartient_arbre(a, 2));
    detruire_arbre(a);

    a = arbre_4();
    assert(!appartient_arbre(a, 0));
    assert(appartient_arbre(a, 5));
    assert(appartient_arbre(a, 20));
    assert(!appartient_arbre(a, 25));
    detruire_arbre(a);

    a = arbre_5();
    assert(!appartient_arbre(a, 0));
    assert(appartient_arbre(a, 5));
    assert(appartient_arbre(a, 51));
    assert(!appartient_arbre(a, 2));
    detruire_arbre(a);
}

void test_struct_arbre(void)
{
    test_egaux_arbre();
    test_appartient_arbre();
}
