#include <stdbool.h>

#include "arbre.h"

#ifndef ARBRE_STD_H
#define ARBRE_STD_H

int taille_arbre(arbre);

int hauteur_arbre(arbre);

bool est_feuille_arbre(arbre);

int nombre_feuilles_arbre(arbre);

bool est_strict_arbre(arbre);

#endif
