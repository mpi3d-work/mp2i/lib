#include <assert.h>

#include "arbre.h"

void test_creer_arbre(void)
{
    arbre a = creer_arbre();
    assert(est_vide_arbre(a));
    detruire_arbre(a);
}

void test_ajouter_arbre(void)
{
    arbre a = ajouter_arbre(
        creer_arbre(),
        creer_arbre(),
        1);
    assert(!est_vide_arbre(a));
    assert(etiquette_arbre(a) == 1);
    assert(est_vide_arbre(gauche_arbre(a)));
    assert(est_vide_arbre(droite_arbre(a)));
    detruire_arbre(a);
}

void test_etiquette_arbre(void)
{
    arbre a = ajouter_arbre(
        creer_arbre(),
        creer_arbre(),
        1);
    assert(etiquette_arbre(a) == 1);
    detruire_arbre(a);
}

void test_gauche_arbre(void)
{
    arbre a;

    a = ajouter_arbre(
        creer_arbre(),
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        1);
    assert(est_vide_arbre(gauche_arbre(a)));
    detruire_arbre(a);

    a = ajouter_arbre(
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        creer_arbre(),
        1);
    assert(!est_vide_arbre(gauche_arbre(a)));
    detruire_arbre(a);
}

void test_droite_arbre(void)
{
    arbre a;

    a = ajouter_arbre(
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        creer_arbre(),
        1);
    assert(est_vide_arbre(droite_arbre(a)));
    detruire_arbre(a);

    a = ajouter_arbre(
        creer_arbre(),
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        1);
    assert(!est_vide_arbre(droite_arbre(a)));
    detruire_arbre(a);
}

void test_est_vide_arbre(void)
{
    arbre a;

    a = creer_arbre();
    assert(est_vide_arbre(a));
    detruire_arbre(a);

    a = ajouter_arbre(
        creer_arbre(),
        creer_arbre(),
        1);
    assert(!est_vide_arbre(a));
    detruire_arbre(a);
}

void test_detruire_arbre(void)
{
    arbre a;

    a = creer_arbre();
    detruire_arbre(a);

    a = ajouter_arbre(
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        creer_arbre(),
        1);
    detruire_arbre(a);
}

void test_arbre(void)
{
    test_creer_arbre();
    test_ajouter_arbre();
    test_etiquette_arbre();
    test_gauche_arbre();
    test_droite_arbre();
    test_est_vide_arbre();
    test_detruire_arbre();
}
