#include "arbre.h"

#ifndef PARCOURS_ARBRE_H
#define PARCOURS_ARBRE_H

void parcours_contenu(contenu c);
// {
//     printf("%d\n", c);
// }

void parcours_profondeur_prefixe(arbre);

void parcours_profondeur_infixe(arbre);

void parcours_profondeur_postfixe(arbre);

void parcours_largeur(arbre);

#endif
