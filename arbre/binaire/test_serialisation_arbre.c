#include <assert.h>
#include <stdio.h>

#include "serialisation_arbre.h"

#include "struct_arbre.h"

#include "test_exemple_arbre.h"

int serialise_contenu(FILE *f, contenu c)
{
    return fprintf(f, "%d\n", c);
}

int deserialise_contenu(FILE *f, contenu *c)
{
    return fscanf(f, "%d\n", c);
}

void test_serialise_arbre(void)
{
    arbre a;
    FILE *f;

    a = arbre_0();
    f = fopen("test_arbre_0.out", "w");
    assert(f != NULL);
    assert(serialise_arbre(f, a) >= 0);
    assert(fclose(f) == 0);
    detruire_arbre(a);

    a = arbre_1();
    f = fopen("test_arbre_1.out", "w");
    assert(f != NULL);
    assert(serialise_arbre(f, a) >= 0);
    assert(fclose(f) == 0);
    detruire_arbre(a);

    a = arbre_2();
    f = fopen("test_arbre_2.out", "w");
    assert(f != NULL);
    assert(serialise_arbre(f, a) >= 0);
    assert(fclose(f) == 0);
    detruire_arbre(a);

    a = arbre_3();
    f = fopen("test_arbre_3.out", "w");
    assert(f != NULL);
    assert(serialise_arbre(f, a) >= 0);
    assert(fclose(f) == 0);
    detruire_arbre(a);

    a = arbre_4();
    f = fopen("test_arbre_4.out", "w");
    assert(f != NULL);
    assert(serialise_arbre(f, a) >= 0);
    assert(fclose(f) == 0);
    detruire_arbre(a);

    a = arbre_5();
    f = fopen("test_arbre_5.out", "w");
    assert(f != NULL);
    assert(serialise_arbre(f, a) >= 0);
    assert(fclose(f) == 0);
    detruire_arbre(a);
}

void test_deserialise_arbre(void)
{
    arbre a, b;
    FILE *f;

    a = arbre_0();
    f = fopen("test_arbre_0.out", "r");
    assert(f != NULL);
    assert(deserialise_arbre(f, &b) == 1);
    assert(egaux_arbre(a, b));
    assert(fclose(f) == 0);
    detruire_arbre(a);
    detruire_arbre(b);

    a = arbre_1();
    f = fopen("test_arbre_1.out", "r");
    assert(f != NULL);
    assert(deserialise_arbre(f, &b) == 1);
    assert(egaux_arbre(a, b));
    assert(fclose(f) == 0);
    detruire_arbre(a);
    detruire_arbre(b);

    a = arbre_2();
    f = fopen("test_arbre_2.out", "r");
    assert(f != NULL);
    assert(deserialise_arbre(f, &b) == 1);
    assert(egaux_arbre(a, b));
    assert(fclose(f) == 0);
    detruire_arbre(a);
    detruire_arbre(b);

    a = arbre_3();
    f = fopen("test_arbre_3.out", "r");
    assert(f != NULL);
    assert(deserialise_arbre(f, &b) == 1);
    assert(egaux_arbre(a, b));
    assert(fclose(f) == 0);
    detruire_arbre(a);
    detruire_arbre(b);

    a = arbre_4();
    f = fopen("test_arbre_4.out", "r");
    assert(f != NULL);
    assert(deserialise_arbre(f, &b) == 1);
    assert(egaux_arbre(a, b));
    assert(fclose(f) == 0);
    detruire_arbre(a);
    detruire_arbre(b);

    a = arbre_5();
    f = fopen("test_arbre_5.out", "r");
    assert(f != NULL);
    assert(deserialise_arbre(f, &b) == 1);
    assert(egaux_arbre(a, b));
    assert(fclose(f) == 0);
    detruire_arbre(a);
    detruire_arbre(b);
}

void test_serialisation_arbre(void)
{
    test_serialise_arbre();
    test_deserialise_arbre();
}
