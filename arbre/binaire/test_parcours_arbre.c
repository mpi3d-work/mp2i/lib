#include <assert.h>
#include <stddef.h>

#include "parcours_arbre.h"

#include "test_exemple_arbre.h"

contenu *order;
int length;
int current;

void parcours_contenu(contenu c)
{
    assert(current < length);
    assert(order[current] == c);
    current += 1;
}

void test_parcours_profondeur_prefixe(void)
{
    arbre a;

    a = arbre_0();
    order = NULL;
    length = 0;
    current = 0;
    parcours_profondeur_prefixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_1();
    order = (contenu[]){1};
    length = 1;
    current = 0;
    parcours_profondeur_prefixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_2();
    order = (contenu[]){1, 2};
    length = 2;
    current = 0;
    parcours_profondeur_prefixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_3();
    order = (contenu[]){1, 2};
    length = 2;
    current = 0;
    parcours_profondeur_prefixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_4();
    order = (contenu[]){10, 4, 3, 8, 5, 20, 15};
    length = 7;
    current = 0;
    parcours_profondeur_prefixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_5();
    order = (contenu[]){10, 4, 3, 8, 5, 55, 20, 15, 51};
    length = 9;
    current = 0;
    parcours_profondeur_prefixe(a);
    assert(current == length);
    detruire_arbre(a);
}

void test_parcours_profondeur_infixe(void)
{
    arbre a;

    a = arbre_0();
    order = NULL;
    length = 0;
    current = 0;
    parcours_profondeur_infixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_1();
    order = (contenu[]){1};
    length = 1;
    current = 0;
    parcours_profondeur_infixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_2();
    order = (contenu[]){2, 1};
    length = 2;
    current = 0;
    parcours_profondeur_infixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_3();
    order = (contenu[]){1, 2};
    length = 2;
    current = 0;
    parcours_profondeur_infixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_4();
    order = (contenu[]){3, 4, 5, 8, 10, 15, 20};
    length = 7;
    current = 0;
    parcours_profondeur_infixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_5();
    order = (contenu[]){3, 4, 5, 8, 55, 10, 15, 20, 51};
    length = 9;
    current = 0;
    parcours_profondeur_infixe(a);
    assert(current == length);
    detruire_arbre(a);
}

void test_parcours_profondeur_postfixe(void)
{
    arbre a;

    a = arbre_0();
    order = NULL;
    length = 0;
    current = 0;
    parcours_profondeur_postfixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_1();
    order = (contenu[]){1};
    length = 1;
    current = 0;
    parcours_profondeur_postfixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_2();
    order = (contenu[]){2, 1};
    length = 2;
    current = 0;
    parcours_profondeur_postfixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_3();
    order = (contenu[]){2, 1};
    length = 2;
    current = 0;
    parcours_profondeur_postfixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_4();
    order = (contenu[]){3, 5, 8, 4, 15, 20, 10};
    length = 7;
    current = 0;
    parcours_profondeur_postfixe(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_5();
    order = (contenu[]){3, 5, 55, 8, 4, 15, 51, 20, 10};
    length = 9;
    current = 0;
    parcours_profondeur_postfixe(a);
    assert(current == length);
    detruire_arbre(a);
}

void test_parcours_largeur(void)
{
    arbre a;

    a = arbre_0();
    order = NULL;
    length = 0;
    current = 0;
    parcours_largeur(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_1();
    order = (contenu[]){1};
    length = 1;
    current = 0;
    parcours_largeur(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_2();
    order = (contenu[]){1, 2};
    length = 2;
    current = 0;
    parcours_largeur(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_3();
    order = (contenu[]){1, 2};
    length = 2;
    current = 0;
    parcours_largeur(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_4();
    order = (contenu[]){10, 4, 20, 3, 8, 15, 5};
    length = 7;
    current = 0;
    parcours_largeur(a);
    assert(current == length);
    detruire_arbre(a);

    a = arbre_5();
    order = (contenu[]){10, 4, 20, 3, 8, 15, 51, 5, 55};
    length = 9;
    current = 0;
    parcours_largeur(a);
    assert(current == length);
    detruire_arbre(a);
}

void test_parcours_arbre(void)
{
    test_parcours_profondeur_prefixe();
    test_parcours_profondeur_infixe();
    test_parcours_profondeur_postfixe();
    test_parcours_largeur();
}
