#include <assert.h>
#include <stdlib.h>

#include "arbre.h"

struct arbre_s
{
    contenu etiquette;
    arbre gauche;
    arbre droite;
};

arbre creer_arbre(void)
{
    return NULL;
}

arbre ajouter_arbre(arbre g, arbre d, contenu c)
{
    arbre n = malloc(sizeof(struct arbre_s));
    assert(n != NULL);

    n->etiquette = c;
    n->gauche = g;
    n->droite = d;

    return n;
}

contenu etiquette_arbre(arbre a)
{
    assert(!est_vide_arbre(a));

    return a->etiquette;
}

arbre gauche_arbre(arbre a)
{
    assert(!est_vide_arbre(a));

    return a->gauche;
}

arbre droite_arbre(arbre a)
{
    assert(!est_vide_arbre(a));

    return a->droite;
}

bool est_vide_arbre(arbre a)
{
    return a == NULL;
}

void detruire_arbre(arbre a)
{
    if (!est_vide_arbre(a))
    {
        detruire_arbre(gauche_arbre(a));
        detruire_arbre(droite_arbre(a));
        free(a);
    }
}
