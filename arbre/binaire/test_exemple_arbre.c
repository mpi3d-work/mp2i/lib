#include "arbre.h"

arbre arbre_0(void)
{
    return creer_arbre();
}

arbre arbre_1(void)
{
    return ajouter_arbre(creer_arbre(), creer_arbre(), 1);
}

arbre arbre_2(void)
{
    return ajouter_arbre(
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        creer_arbre(),
        1);
}

arbre arbre_3(void)
{
    return ajouter_arbre(
        creer_arbre(),
        ajouter_arbre(
            creer_arbre(),
            creer_arbre(),
            2),
        1);
}

arbre arbre_4(void)
{
    return ajouter_arbre(
        ajouter_arbre(
            ajouter_arbre(
                creer_arbre(),
                creer_arbre(),
                3),
            ajouter_arbre(
                ajouter_arbre(
                    creer_arbre(),
                    creer_arbre(),
                    5),
                creer_arbre(),
                8),
            4),
        ajouter_arbre(
            ajouter_arbre(
                creer_arbre(),
                creer_arbre(),
                15),
            creer_arbre(),
            20),
        10);
}

arbre arbre_5(void)
{
    return ajouter_arbre(
        ajouter_arbre(
            ajouter_arbre(
                creer_arbre(),
                creer_arbre(),
                3),
            ajouter_arbre(
                ajouter_arbre(
                    creer_arbre(),
                    creer_arbre(),
                    5),
                ajouter_arbre(
                    creer_arbre(),
                    creer_arbre(),
                    55),
                8),
            4),
        ajouter_arbre(
            ajouter_arbre(
                creer_arbre(),
                creer_arbre(),
                15),
            ajouter_arbre(
                creer_arbre(),
                creer_arbre(),
                51),
            20),
        10);
}
