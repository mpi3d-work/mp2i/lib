let rec egaux a b =
  match (a, b) with
  | Arbre.Noeud (e1, a1, b1), Arbre.Noeud (e2, a2, b2) ->
      e1 = e2 && egaux a1 a2 && egaux b1 b2
  | Arbre.Vide, Arbre.Vide -> true
  | _ -> false
(* Pour les flemmards *)
(* let egaux a b = a = b *)

let rec appartient a x =
  match a with
  | Arbre.Noeud (e, a, b) -> e = x || appartient a x || appartient b x
  | Arbre.Vide -> false
