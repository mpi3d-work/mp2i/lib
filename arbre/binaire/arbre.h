#include <stdbool.h>

#ifndef ARBRE_H
#define ARBRE_H

typedef int contenu;

typedef struct arbre_s *arbre;

arbre creer_arbre(void);

arbre ajouter_arbre(arbre gauche, arbre droite, contenu);

contenu etiquette_arbre(arbre);

arbre gauche_arbre(arbre);

arbre droite_arbre(arbre);

bool est_vide_arbre(arbre);

void detruire_arbre(arbre);

#endif
