let rec taille = function
  | Arbre.Noeud (_, a, b) -> 1 + taille a + taille b
  | Arbre.Vide -> 0

let max a b = if a > b then a else b

let rec hauteur = function
  | Arbre.Noeud (_, a, b) -> 1 + max (hauteur a) (hauteur b)
  | Arbre.Vide -> 0

let est_feuille = function
  | Arbre.Noeud (_, a, b) -> a = Arbre.Vide && b = Arbre.Vide
  | Arbre.Vide -> false

let rec nombre_feuilles = function
  | Arbre.Noeud (_, a, b) ->
      if a = Arbre.Vide && b = Arbre.Vide then 1
      else nombre_feuilles a + nombre_feuilles b
  | Arbre.Vide -> 0

let rec est_strict = function
  | Arbre.Noeud (_, a, b) ->
      if a = Arbre.Vide && b = Arbre.Vide then true
      else est_strict a && est_strict b
  | Arbre.Vide -> false
