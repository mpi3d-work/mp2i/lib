#include "struct_arbre.h"

bool egaux_arbre(arbre a, arbre b)
{
    bool x = est_vide_arbre(a);
    bool y = est_vide_arbre(b);
    if (x || y)
        return x && y;
    return egaux_contenu(etiquette_arbre(a), etiquette_arbre(b)) &&
           egaux_arbre(gauche_arbre(a), gauche_arbre(b)) &&
           egaux_arbre(droite_arbre(a), droite_arbre(b));
}

bool appartient_arbre(arbre a, contenu c)
{
    if (est_vide_arbre(a))
        return false;
    return egaux_contenu(etiquette_arbre(a), c) ||
           appartient_arbre(gauche_arbre(a), c) ||
           appartient_arbre(droite_arbre(a), c);
}
