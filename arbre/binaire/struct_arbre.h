#include <stdbool.h>

#include "arbre.h"

#ifndef STRUCT_ARBRE_H
#define STRUCT_ARBRE_H

bool egaux_contenu(contenu a, contenu b);
// {
//     return a == b;
// }

bool egaux_arbre(arbre a, arbre b);

bool appartient_arbre(arbre, contenu);

#endif
