#include <stdio.h>

#include "arbre.h"

#ifndef SERIALISATION_ARBRE_H
#define SERIALISATION_ARBRE_H

int serialise_contenu(FILE *f, contenu c);
// {
//     return fprintf(f, "%d\n", c);
// }

int deserialise_contenu(FILE *f, contenu *c);
// {
//     return fscanf(f, "%d\n", c);
// }

int serialise_arbre(FILE *, arbre);

int deserialise_arbre(FILE *, arbre *);

#endif
