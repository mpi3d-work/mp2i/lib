let rec partition = function
  | x :: y :: l ->
      let a, b = partition l in
      (x :: a, y :: b)
  | l -> (l, [])

let rec fusion l1 l2 =
  match (l1, l2) with
  | x :: a, y :: b -> if x > y then y :: fusion b l1 else x :: fusion a l2
  | l, [] | [], l -> l

let rec tri_fusion = function
  | [] -> []
  | [ x ] -> [ x ]
  | l ->
      let a, b = partition l in
      fusion (tri_fusion a) (tri_fusion b)

let rec partition_pivot p = function
  | x :: l ->
      let a, b = partition_pivot p l in
      if x > p then (a, x :: b) else (x :: a, b)
  | [] -> ([], [])

let rec tri_rapide = function
  | p :: l ->
      let a, b = partition_pivot p l in
      tri_rapide a @ [ p ] @ tri_rapide b
  | [] -> []

let rec extrait_optimise = function
  | x :: y :: l ->
      let l = y :: l in
      if x > y then ([ x ], l)
      else
        let s, l = extrait_optimise l in
        (x :: s, l)
  | l -> (l, [])

let rec partition_optimise = function
  | [] -> []
  | l ->
      let s, l = extrait_optimise l in
      s :: partition_optimise l

let rec fusion_optimise l1 l2 =
  match (l1, l2) with
  | x :: a, y :: b ->
      if x > y then y :: fusion_optimise b l1 else x :: fusion_optimise a l2
  | l, [] | [], l -> l

let rec compresse_optimise = function
  | x :: y :: l -> fusion_optimise x y :: compresse_optimise l
  | l -> l

let tri_fusion_optimise l =
  let rec tri = function
    | [] -> []
    | [ x ] -> x
    | l -> tri (compresse_optimise l)
  in
  tri (partition_optimise l)
