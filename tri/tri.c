#include "tri.h"

int partition_lomuto(contenu t[], int l)
{
    contenu p = t[0];
    int j = 0;
    for (int i = 1; i < l; i += 1)
    {
        contenu v = t[i];
        if (comparaison(v, p) < 0)
        {
            j += 1;
            t[i] = t[j];
            t[j] = v;
        }
    }
    t[0] = t[j];
    t[j] = p;
    return j;
}

int partition_hoare(contenu t[], int l)
{
    contenu p = t[0];
    int i = 0;
    int j = l - 1;
    while (i < j)
    {
        while (comparaison(t[i], p) < 0)
            i += 1;
        while (comparaison(t[j], p) > 0)
            j -= 1;

        // Si il y a des doublons
        // if (comparaison(t[i], p) == 0 && comparaison(t[j], p) == 0)
        // {
        //     j -= 1;
        //     continue;
        // }

        contenu v = t[i];
        t[i] = t[j];
        t[j] = v;
    }
    return i;
}

void tri_rapide(contenu t[], int l)
{
    if (l > 1)
    {
        int i = pivot(l);
        contenu p = t[i];
        t[i] = t[0];
        t[0] = p;
        i = partition(t, l);
        tri_rapide(t, i);
        i += 1;
        tri_rapide(t + i, l - i);
    }
}
