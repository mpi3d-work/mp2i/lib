#include <stdlib.h>

#ifndef TRI_H
#define TRI_H

typedef int contenu;

int comparaison(contenu a, contenu b)
{
    return a - b;
}

int pivot(int longeur)
{
    return rand() / (RAND_MAX / longeur); // Aleatoire
    // return 0; // Premier
    // return taille / 2; // Milieu
    // return taille - 1; // Dernier
}

int partition_lomuto(contenu t[], int longeur);
int partition_hoare(contenu t[], int longeur);

int partition(contenu t[], int longeur)
{
    return partition_lomuto(t, longeur); // Doublons
    // return partition_hoare(t, l); // Pas de doublons
}

void tri_rapide(contenu t[], int longeur);

#endif
