#include <stdbool.h>

#ifndef BASE_H
#define BASE_H

int decompose_2(int n, bool out[]);
int recompose_2(bool in[], int l);

int decompose(int b, int n, int out[]);
int recompose(int b, int in[], int l);

#endif
