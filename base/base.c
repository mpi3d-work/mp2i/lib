#include "base.h"

bool itob(int i)
{
    return i != 0;
}

int btoi(bool b)
{
    if (b)
        return 1;
    else
        return 0;
}

int decompose_2(int n, bool out[])
{
    int i = 0;
    while (n > 0)
    {
        out[i] = itob(n & 1);
        n >>= 1;
        i += 1;
    }
    return i;
}

int recompose_2(bool in[], int l)
{
    int n = 0;
    for (int i = l - 1; i >= 0; i -= 1)
    {
        n <<= 1;
        n |= btoi(in[i]);
    }
    return n;
}

int decompose(int b, int n, int out[])
{
    int i = 0;
    while (n > 0)
    {
        out[i] = n % b;
        n /= b;
        i += 1;
    }
    return i;
}

int recompose(int b, int in[], int l)
{
    int n = 0;
    for (int i = l - 1; i >= 0; i -= 1)
    {
        n *= b;
        n += in[i];
    }
    return n;
}
