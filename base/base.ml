let bool_of_int i = i <> 0
let int_of_bool b = if b then 1 else 0

let rec decompose_2 = function
  | 0 -> []
  | n -> bool_of_int (n land 1) :: decompose_2 (n lsr 1)

let rec recompose_2 = function
  | x :: l -> (recompose_2 l lsl 1) lor int_of_bool x
  | [] -> 0

let rec decompose b = function 0 -> [] | n -> (n mod b) :: decompose b (n / b)
let rec recompose b = function x :: l -> (recompose b l * b) + x | [] -> 0
