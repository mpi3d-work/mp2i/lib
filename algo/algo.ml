let rec factorielle = function 0 -> 1 | n -> n * factorielle (n - 1)

let rec fibonacci = function
  | n when n > 1 -> fibonacci (n - 1) + fibonacci (n - 2)
  | n -> n

let rec binomial n = function
  | k when k = 0 || k = n -> 1
  | k ->
      let n = n - 1 in
      binomial n k + binomial n (k - 1)

let fibonacci_optimise n =
  let rec f = function
    | n when n > 1 ->
        let a, b = f (n - 1) in
        (a + b, a)
    | n -> (n, 0)
  in
  fst (f n)

let rec incremente_optimise = function
  | x :: l ->
      let l, y = incremente_optimise l in
      let x = x + y in
      (x :: l, x)
  | l -> (l, 1)

let rec initialise_optimise = function
  | 0 -> []
  | n -> 1 :: initialise_optimise (n - 1)

let binomial_optimise n k =
  let rec b n = function
    | 0 -> (initialise_optimise n, 1)
    | k ->
        let l, _ = b n (k - 1) in
        incremente_optimise l
  in
  let n = n - k in
  let _, x = if n > k then b k n else b n k in
  x
