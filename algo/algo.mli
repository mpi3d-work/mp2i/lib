val factorielle : int -> int
val fibonacci : int -> int
val binomial : int -> int -> int
val fibonacci_optimise : int -> int
val binomial_optimise : int -> int -> int
