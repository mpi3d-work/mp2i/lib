let test_factorielle () =
  assert (Algo.factorielle 0 = 1);
  assert (Algo.factorielle 1 = 1);
  assert (Algo.factorielle 2 = 2);
  assert (Algo.factorielle 3 = 6);
  assert (Algo.factorielle 11 = 39916800)

let test_fibonacci () =
  assert (Algo.fibonacci 0 = 0);
  assert (Algo.fibonacci 1 = 1);
  assert (Algo.fibonacci 2 = 1);
  assert (Algo.fibonacci 3 = 2);
  assert (Algo.fibonacci 4 = 3);
  assert (Algo.fibonacci 5 = 5);
  assert (Algo.fibonacci 15 = 610)

let test_binomial () =
  assert (Algo.binomial 0 0 = 1);
  assert (Algo.binomial 1 0 = 1);
  assert (Algo.binomial 2 1 = 2);
  assert (Algo.binomial 2 2 = 1);
  assert (Algo.binomial 6 3 = 20);
  assert (Algo.binomial 7 2 = 21);
  assert (Algo.binomial 8 3 = 56);
  assert (Algo.binomial 17 0 = 1);
  assert (Algo.binomial 28 1 = 28);
  assert (Algo.binomial 26 7 = 657800);
  assert (Algo.binomial 29 23 = 475020)

let test_fibonacci_optimise () =
  assert (Algo.fibonacci_optimise 0 = 0);
  assert (Algo.fibonacci_optimise 1 = 1);
  assert (Algo.fibonacci_optimise 2 = 1);
  assert (Algo.fibonacci_optimise 3 = 2);
  assert (Algo.fibonacci_optimise 4 = 3);
  assert (Algo.fibonacci_optimise 5 = 5);
  assert (Algo.fibonacci_optimise 15 = 610)

let test_binomial_optimise () =
  assert (Algo.binomial_optimise 0 0 = 1);
  assert (Algo.binomial_optimise 1 0 = 1);
  assert (Algo.binomial_optimise 2 1 = 2);
  assert (Algo.binomial_optimise 2 2 = 1);
  assert (Algo.binomial_optimise 6 3 = 20);
  assert (Algo.binomial_optimise 7 2 = 21);
  assert (Algo.binomial_optimise 8 3 = 56);
  assert (Algo.binomial_optimise 17 0 = 1);
  assert (Algo.binomial_optimise 28 1 = 28);
  assert (Algo.binomial_optimise 26 7 = 657800);
  assert (Algo.binomial_optimise 29 23 = 475020)

let test_algo () =
  test_factorielle ();
  test_fibonacci ();
  test_binomial ();
  test_fibonacci_optimise ();
  test_binomial_optimise ()
