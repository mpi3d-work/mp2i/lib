#ifndef ALGO_H
#define ALGO_H

int factorielle(int n);
int fibonacci(int n);
int binomial(int n, int k);

int fibonacci_optimise(int n);
int binomial_optimise(int n, int k);

#endif
