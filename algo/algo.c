#include <assert.h>
#include <stdlib.h>

int factorielle(int n)
{
    int r = 1;
    for (int i = 1; i <= n; i += 1)
        r *= i;
    return r;
}

int fibonacci(int n)
{
    if (n > 1)
        return fibonacci(n - 1) + fibonacci(n - 2);

    return n;
}

int binomial(int n, int k)
{
    if (k == 0 || k == n)
        return 1;

    n -= 1;
    return binomial(n, k) + binomial(n, k - 1);
}

int fibonacci_optimise(int n)
{
    int a = 0, b = 1;
    for (int i = 0; i < n; i += 1)
    {
        int c = a + b;
        a = b;
        b = c;
    }
    return a;
}

int binomial_optimise(int n, int k)
{
    n -= k;
    if (k < n)
    {
        int t = k;
        k = n;
        n = t;
    }

    n += 1;
    int *v = malloc(n * sizeof(int));
    assert(v != NULL);
    for (int i = 0; i < n; i += 1)
        v[i] = 1;

    for (int i = 0; i < k; i += 1)
        for (int j = 1; j < n; j += 1)
            v[j] += v[j - 1];

    int c = v[n - 1];
    free(v);
    return c;
}
