#include <assert.h>

#include "algo.h"

void test_factorielle(void)
{
    assert(factorielle(0) == 1);
    assert(factorielle(1) == 1);
    assert(factorielle(2) == 2);
    assert(factorielle(3) == 6);
    assert(factorielle(11) == 39916800);
}

void test_fibonacci(void)
{
    assert(fibonacci(0) == 0);
    assert(fibonacci(1) == 1);
    assert(fibonacci(2) == 1);
    assert(fibonacci(3) == 2);
    assert(fibonacci(4) == 3);
    assert(fibonacci(5) == 5);
    assert(fibonacci(15) == 610);
}

void test_binomial(void)
{
    assert(binomial(0, 0) == 1);
    assert(binomial(1, 0) == 1);
    assert(binomial(2, 1) == 2);
    assert(binomial(2, 2) == 1);
    assert(binomial(6, 3) == 20);
    assert(binomial(7, 2) == 21);
    assert(binomial(8, 3) == 56);
    assert(binomial(17, 0) == 1);
    assert(binomial(28, 1) == 28);
    assert(binomial(26, 7) == 657800);
    assert(binomial(29, 23) == 475020);
}

void test_fibonacci_optimise(void)
{
    assert(fibonacci_optimise(0) == 0);
    assert(fibonacci_optimise(1) == 1);
    assert(fibonacci_optimise(2) == 1);
    assert(fibonacci_optimise(3) == 2);
    assert(fibonacci_optimise(4) == 3);
    assert(fibonacci_optimise(5) == 5);
    assert(fibonacci_optimise(15) == 610);
}

void test_binomial_optimise(void)
{
    assert(binomial_optimise(0, 0) == 1);
    assert(binomial_optimise(1, 0) == 1);
    assert(binomial_optimise(2, 1) == 2);
    assert(binomial_optimise(2, 2) == 1);
    assert(binomial_optimise(6, 3) == 20);
    assert(binomial_optimise(7, 2) == 21);
    assert(binomial_optimise(8, 3) == 56);
    assert(binomial_optimise(17, 0) == 1);
    assert(binomial_optimise(28, 1) == 28);
    assert(binomial_optimise(26, 7) == 657800);
    assert(binomial_optimise(29, 23) == 475020);
}

void test_algo(void)
{
    test_factorielle();
    test_fibonacci();
    test_binomial();
    test_fibonacci_optimise();
    test_binomial_optimise();
}
