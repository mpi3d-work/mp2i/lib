#include <stdbool.h>

#ifndef LISTE_H
#define LISTE_H

typedef int contenu;

typedef struct liste_s *liste;

liste creer_liste(void);

liste ajouter_liste(liste, contenu);

contenu tete_liste(liste);

liste queue_liste(liste);

bool est_vide_liste(liste);

void detruire_liste(liste);

#endif
