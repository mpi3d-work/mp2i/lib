#include <assert.h>
#include <stdlib.h>

#include "liste.h"

struct liste_s
{
    contenu tete;
    struct liste_s *queue;
};

liste creer_liste(void)
{
    return NULL;
}

liste ajouter_liste(liste l, contenu c)
{
    liste m = malloc(sizeof(struct liste_s));
    assert(m != NULL);

    m->tete = c;
    m->queue = l;

    return m;
}

contenu tete_liste(liste l)
{
    assert(!est_vide_liste(l));

    return l->tete;
}

liste queue_liste(liste l)
{
    assert(!est_vide_liste(l));

    return l->queue;
}

bool est_vide_liste(liste l)
{
    return l == NULL;
}

void detruire_liste(liste l)
{
    if (!est_vide_liste(l))
    {
        detruire_liste(queue_liste(l));
        free(l);
    }
}
