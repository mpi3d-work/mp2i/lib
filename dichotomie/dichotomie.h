#ifndef DICHOTOMIE_H
#define DICHOTOMIE_H

typedef int contenu;

int comparaison(contenu a, contenu b)
{
    return a - b;
}

int recherche_dichotomique(contenu val, contenu t[], int longeur);

#endif
