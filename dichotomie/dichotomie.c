#include "dichotomie.h"

int recherche_dichotomique(contenu v, contenu t[], int l)
{
    int a = 0;
    int b = l - 1;
    while (a <= b)
    {
        int m = (a + b) / 2;
        int c = comparaison(t[m], v);
        if (c > 0)
            b = m - 1;
        else if (c < 0)
            a = m + 1;
        else
            return m;
    }
    return -1;
}
