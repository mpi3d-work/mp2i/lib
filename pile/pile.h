#include <stdbool.h>

#ifndef PILE_H
#define PILE_H

typedef int contenu;

typedef struct pile_s *pile;

pile creer_pile(void);

void empiler_pile(pile, contenu);

contenu depiler_pile(pile);

bool est_vide_pile(pile);

void detruire_pile(pile);

#endif
