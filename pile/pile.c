#include <assert.h>
#include <stdlib.h>

#include "pile.h"

struct maillon_s
{
    contenu valeur;
    struct maillon_s *suivant;
};

typedef struct maillon_s *maillon;

struct pile_s
{
    maillon premier;
};

pile creer_pile(void)
{
    pile p = malloc(sizeof(struct pile_s));
    assert(p != NULL);

    p->premier = NULL;

    return p;
}

void empiler_pile(pile p, contenu c)
{
    maillon m = malloc(sizeof(struct maillon_s));
    assert(m != NULL);

    m->valeur = c;
    m->suivant = p->premier;

    p->premier = m;
}

contenu depiler_pile(pile p)
{
    assert(!est_vide_pile(p));

    maillon m = p->premier;
    contenu c = m->valeur;

    p->premier = m->suivant;

    free(m);

    return c;
}

bool est_vide_pile(pile p)
{
    return p->premier == NULL;
}

void detruire_pile(pile p)
{
    while (!est_vide_pile(p))
        depiler_pile(p);

    free(p);
}
