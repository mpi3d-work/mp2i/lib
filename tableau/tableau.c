#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "tableau.h"

struct tableau_s
{
    contenu *donnees;
    int longueur;
};

tableau creer_tableau(int l, contenu c)
{
    contenu *d = NULL;
    if (l > 0)
    {
        d = malloc(l * sizeof(contenu));
        assert(d != NULL);

        for (int i = 0; i < l; i += 1)
            d[i] = c;
    }

    tableau t = malloc(sizeof(struct tableau_s));
    assert(t != NULL);

    t->donnees = d;
    t->longueur = l;

    return t;
}

bool est_indice_tableau(tableau t, int i)
{
    return 0 <= i && i < t->longueur;
}

contenu acceder_tableau(tableau t, int i)
{
    assert(est_indice_tableau(t, i));

    return t->donnees[i];
}

int longueur_tableau(tableau t)
{
    return t->longueur;
}

void modifier_tableau(tableau t, int i, contenu c)
{
    assert(est_indice_tableau(t, i));

    t->donnees[i] = c;
}

void detruire_tableau(tableau t)
{
    contenu *d = t->donnees;
    if (d != NULL)
        free(d);

    free(t);
}
