#include <stdbool.h>

#ifndef TABLEAU_H
#define TABLEAU_H

typedef int contenu;

typedef struct tableau_s *tableau;

tableau creer_tableau(int longueur, contenu);

contenu acceder_tableau(tableau, int indice);

int longueur_tableau(tableau);

void modifier_tableau(tableau, int indice, contenu);

void detruire_tableau(tableau);

#endif
