let rec pgcd a b = if a = 0 then b else pgcd (b mod a) a
