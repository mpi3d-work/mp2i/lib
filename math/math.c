#include "math.h"

int pgcd(int a, int b)
{
    while (a > 0)
    {
        int r = b % a;
        b = a;
        a = r;
    }
    return b;
}
